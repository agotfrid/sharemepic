class PagesController < ApplicationController
  def home
  end

  def about
  end

  def search
    @quotes = Quote.where("quote_author LIKE ?", "%" + params[:author] + "%")
  end
end
