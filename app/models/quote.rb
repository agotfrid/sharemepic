class Quote < ApplicationRecord
  belongs_to :post

  validates :quote_text, :quote_author, presence: true
  validates_associated :post

end
