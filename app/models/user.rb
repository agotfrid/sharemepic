class User < ApplicationRecord
  has_many :user_posts
  has_many :posts, through: :user_posts

  validates :username, length: { minimum: 5 }, presence: true
  validates :email, :first_name, :last_name, presence: true
  validates_associated :posts
end
