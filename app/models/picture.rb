class Picture < ApplicationRecord
  has_many :post_pictures
  has_many :posts, through: :post_pictures

  validates :picture_title, :picture_url, presence: true
  validates_associated :posts
end
