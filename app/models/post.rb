class Post < ApplicationRecord
  has_many :post_pictures
  has_many :pictures, through: :post_pictures
  has_one :user_post
  has_one :user, through: :user_post
  has_many :quotes

  validates :post_title, :post_text, presence: true
  validates_associated :quotes, :pictures
end
