class PostPicture < ApplicationRecord
  belongs_to :post
  belongs_to :picture

  validates_associated :post, :picture
end
