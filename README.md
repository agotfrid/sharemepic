# Show Me Pic

## Rails app for collecting pictures and inspirational quotes from API

* Built with Ruby v 2.7.1 

### 1.1 Describe Datasets
- Pictures
    - [Lorem Picsum](https://picsum.photos/)
    - Picture url data is pulled from the api and helps populate the pictures table
    - Picture table has columns 
        - ID[PK] 
        - picture_title
        - picture_url
- Quotes
    - [Type.fit](https://type.fit/api/quotes)
    - Quote text is being pulled from the api and helps populate the quotes table
    - Quotes table has the following columns
        - ID[PK]
        - post[FK]
        - quote_text
        - quote_author
- User Data
    - [Faker Gem](https://github.com/faker-ruby/faker/blob/master/doc/default/name.md)
    - User table is being populated using the faker gem
    - User table has the following columns
        - ID[PK]
        - username
        - first_name
        - last_name
        - email
        

### 1.2 ERD Diagram
![ERD Diagram](/extras/ShareMePicERD.png)

### 1.3 Generating Active Record Models and Tables
    # Create Model
    rails g model User username:string first_name:string last_name:string email:string
    rails g model Picture picture_title:string picture_url:string
    rails g model Quote quote_text:string quote_url:string
    rails g model Post post_title:string post_text:text quote:references
    # Create Relationship Models
    rails g model UserPost user:references post:references
    rails g model PostPicture post:references picture:references


