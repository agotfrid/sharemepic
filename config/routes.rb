Rails.application.routes.draw do
  root 'pages#home'
  get 'about', to: 'pages#about'
  get 'posts', to: 'posts#index'
  get 'posts/show'
  get 'search', to: 'pages#search'
  # get 'posts/new'
  # get 'posts/create'
  # get 'posts/edit'
  # get 'posts/update'
  # get 'posts/destroy'
  get 'users', to: 'users#index'
  get 'users/show'
  get 'pictures', to: 'pictures#index'
  get 'pictures/show'
  get 'quotes', to: 'quotes#index'
  get 'quotes/show'
  resources :posts, :pictures, :quotes, :users
end
