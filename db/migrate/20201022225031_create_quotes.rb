class CreateQuotes < ActiveRecord::Migration[6.0]
  def change
    create_table :quotes do |t|
      t.string :quote_text
      t.string :quote_author
      t.references :post, null: false, foreign_key: true

      t.timestamps
    end
  end
end
