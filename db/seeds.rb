User.destroy_all
Post.destroy_all
Quote.destroy_all
Picture.destroy_all
UserPost.destroy_all
PostPicture.destroy_all
require "faker"

REPEATS = 50

quote_url = URI("https://type.fit/api/quotes")
quote_request = Net::HTTP.get(quote_url)
quote_response = JSON.parse(quote_request)

REPEATS.times do |i|
  first_name = Faker::Name.first_name
  last_name = Faker::Name.last_name
  email = "#{first_name + last_name}@email.com"
  User.create!(
    username:   (first_name + last_name),
    first_name: first_name,
    last_name:  last_name,
    email:      email
  )

  text = quote_response[i]["text"]
  author = quote_response[i]["author"]
  author = "N/A" if author.nil?
  created_post = Post.create!(post_title: Faker::Lorem.sentence(word_count: 4),
                              post_text:  Faker::Lorem.paragraph(sentence_count: 6))
  created_post.quotes.create!(
    quote_text:   text,
    quote_author: author
  )
end

2.times do |page|
  url = URI("https://picsum.photos/v2/list?limit=100&page=#{page}")
  request = Net::HTTP.get(URI(url))
  response = JSON.parse(request)
  REPEATS.times do |i|
    id = response[i]["id"]
    title = "#{response[i]['author']} #{response[i]['id']}"
    picture_url = "https://picsum.photos/id/#{id}/1000/1000"
    Picture.create!(
      picture_title: title,
      picture_url:   picture_url
    )
  end
end
